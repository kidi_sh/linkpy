import cookielib,json
import os
import urllib
import urllib2
import re
import string,requests
from bs4 import BeautifulSoup

""" fill in your linkedin information """

username = "xxx"
password = "xxxx"

cookie_filename = "parser.cookies.txt"

class LinkedInParser(object):

    def __init__(self, login, password):
        """ Start up... """
        self.login = login
        self.password = password

        # Simulate browser with cookies enabled
        self.cj = cookielib.MozillaCookieJar(cookie_filename)
        if os.access(cookie_filename, os.F_OK):
            self.cj.load()
        self.opener = urllib2.build_opener(
            urllib2.HTTPRedirectHandler(),
            urllib2.HTTPHandler(debuglevel=0),
            urllib2.HTTPSHandler(debuglevel=0),
            urllib2.HTTPCookieProcessor(self.cj)
        )
        self.opener.addheaders = [
            ('User-agent', ('Mozilla/4.0 (compatible; MSIE 6.0; '
                           'Windows NT 5.2; .NET CLR 1.1.4322)'))
        ]

        # Login
        self.loginPage()

        title = self.loadTitle()
        print title

        self.cj.save()


    def loadPage(self, url, data=None):
        """
        Utility function to load HTML from URLs for us with hack to continue despite 404
        """
        # We'll print the url in case of infinite loop
        # print "Loading URL: %s" % url
        try:
            if data is not None:
                response = self.opener.open(url, data)
            else:
                response = self.opener.open(url)
            return ''.join(response.readlines())
        except:
            # If URL doesn't load for ANY reason, try again...
            # Quick and dirty solution for 404 returns because of network problems
            # However, this could infinite loop if there's an actual problem
            return self.loadPage(url, data)

    def loginPage(self):
        """
        Handle login. This should populate our cookie jar.
        """
        html = self.loadPage("https://www.linkedin.com/uas/login")
        soup = BeautifulSoup(html)
        csrf = soup.find(id="loginCsrfParam-login")['value']
	csrftoken = soup.find(id="csrfToken-login")['value']

        login_data = urllib.urlencode({
            'session_key': self.login,
            'session_password': self.password,
            'loginCsrfParam': csrf,
	   # 'csrfToken':csrftoken
        })

        html = self.loadPage("https://www.linkedin.com/uas/login-submit", login_data)
#        return

    """define grab detail function"""
    def grabdetail(link):
	url = self.loadpage(link)
    url3 = self.loadPage(link)
    soup2 = BeautifulSoup(url3)
    for title in soup2.findAll('a',{'name':'exportToPdf'}):
        print(title.get('href'))
        uri =title.get('href')
        self.loadPage("http://www.linkedin.com"+uri)

    def loadTitle(self):
        html = self.loadPage("http://www.linkedin.com/nhome")
	
	#url to search people
	url2 = self.loadPage("https://www.linkedin.com/vsearch/p?orig=TRNV&rsid=2736836231473749355586&keywords=product+manager&trk=vsrp_people_sel&trkInfo=VSRPsearchId%3A2736836231473749355586%2CVSRPcmpt%3Atrans_nav")
	soup = BeautifulSoup(url2)
	for test in soup.findAll(id="voltron_srp_main-content"):
		#get hidden json object
		data = json.loads(str(test.contents[0].encode('utf-8')))
#		print(json.loads(str(test.contents[0])))
		#person1 = data['content']['page']['voltron_unified_search_json']['search']['results'][1]['person']['lastName']
		dperson = data['content']['page']['voltron_unified_search_json']['search']['results']
		print("name: "+dperson[1]['person']['lastName'])
		"""
	
		(WARNING) HARDCODED-> link to be send to grab detail function
		-> change the iteration/loop below the link if possible
	
		"""
		link1 = dperson[1]['person']['link_nprofile_view_4'] 	
                link2 = dperson[2]['person']['link_nprofile_view_4'] 
                link3 = dperson[3]['person']['link_nprofile_view_4'] 
                link4 = dperson[5]['person']['link_nprofile_view_4'] 
                link5 = dperson[6]['person']['link_nprofile_view_4'] 
                link6 = dperson[7]['person']['link_nprofile_view_4'] 
                link7 = dperson[8]['person']['link_nprofile_view_4'] 
                link8 = dperson[9]['person']['link_nprofile_view_4'] 
                link9 = dperson[10]['person']['link_nprofile_view_4'] 
                link10 = dperson[11]['person']['link_nprofile_view_4'] 
		print len(dperson)
		arr = []
		""" 
		looping for above function, but still got error(loop over multidimensional array)
		 
		for i in xrange(1, len(dperson)):
			arr = arr.append( dperson[i]['person'])
		print len(arr)
		"""


parser = LinkedInParser(username, password)
